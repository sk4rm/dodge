extends KinematicBody2D

const UP = Vector2(0, -1)
const SPEED = 200
var motion = Vector2(0, 0)


func _physics_process(_delta):
	
	# Input start
	if Input.is_action_pressed("ui_left"):
		$Sprite.flip_h = true
		motion.x = -SPEED
	elif Input.is_action_pressed("ui_right"):
		$Sprite.flip_h = false
		motion.x = SPEED
	else:
		motion.x = 0
	
	if Input.is_action_pressed("ui_up"):
		motion.y = -SPEED
	elif Input.is_action_pressed("ui_down"):
		motion.y = SPEED
	else:
		motion.y = 0
	# Input end
	if motion.x or motion.y:
		$Sprite.play("run")
	else:
		$Sprite.play("idle")
	
	motion = move_and_slide(motion, UP)
